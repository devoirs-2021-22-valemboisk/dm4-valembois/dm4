# EXERCICE1

def bin2dec(b):
    resultat=0
    for i in range(8):
        resultat += int(b[i])*2**(7-i)
    return resultat
    
    
def XOR(a,b):
    if a==b:
        return "0"
    else:
        return "1"
    
def XOR_octet(o,p):
    chaine = ""
    for i in range(8):
        chaine += XOR(o[i],p[i])
    return chaine
        
def mot_en_binaire(m):
    tab=[]
    for lettre in m:
        tab.append(bin(ord(lettre))[2:].zfill(8))
    return tab

def c_XOR(m,c):
    resultat_bin = []
    m_bin = mot_en_binaire(m)
    c_bin = mot_en_binaire(c)
    for i in range(len(m)):
        resultat_bin.append(XOR_octet(m_bin[i],c_bin[i%len(c)]))
    chaine=""
    for element in resultat_bin:
        chaine += chr(bin2dec(element))
    return chaine

print(c_XOR("\x14.6=s(4$9n';-45§s%#a1§#&,2&o","',&<:*"))
